﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName="GameState", menuName="States/GameState")]
public class GameState : ScriptableObject
{
    public string currentCheckpointGUID;

    void Reset()
    {
        currentCheckpointGUID = "";
    }
}
