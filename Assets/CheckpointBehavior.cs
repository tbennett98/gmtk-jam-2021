﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckpointBehavior : MonoBehaviour
{
    public GameState gameState;
    public GameObject checkpointGetAnimPrefab;
    public string checkpointGUID;

    void Awake()
    {
        //
        // Generate guid and see if that is the one the gamestate contains
        //
        checkpointGUID =
            $"{SceneManager.GetActiveScene().buildIndex}:Checkpoint:{gameObject.name}:{transform.position.x}:{transform.position.y}:{transform.position.z}";

        if (gameState.currentCheckpointGUID == checkpointGUID)
        {
            GameObject player = GameObject.FindWithTag("Player");
            if (player != null)
            {
                player.transform.position = transform.position;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.attachedRigidbody.CompareTag("Player")) return;      // NOTE: this time we do want to have checkpoints be reachable via the blue blocks
        if (gameState.currentCheckpointGUID == checkpointGUID) return;  // Only do if the checkpoint has changed

        gameState.currentCheckpointGUID = checkpointGUID;
        StartCoroutine(PlayCheckpointUIAnim());
    }

    private IEnumerator PlayCheckpointUIAnim()
    {
        GameObject uiAnimObj = Instantiate(checkpointGetAnimPrefab, Vector3.zero, Quaternion.identity);
        yield return new WaitForSecondsRealtime(1.5f);
        Destroy(uiAnimObj);
    }
}
