﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public GameObject introTransition;
    private Transform mainCamera;
    private Rigidbody body;
    private PlayerAnimateManager playerAnimateManager;
    private OtherBodyLinesHolder otherBodyLinesHolder;

    [Header("Movement Props")]
    public float movementMaxXZSpeed = 10f;
    public float movementAcceleration = 2f;
    public float movementAccelerationMidair = 2f;
    public float movementDeceleration = 2f;
    public float jumpSpeed = 20f;
    public float releaseJumpDamperFactor = 0.7f;
    private bool inputJumpDown = false;
    private bool inputJumpUp = false;

    [Header("On ground detection")]
    public bool isGrounded = false;
    public float checkBoxHeight = 0.1f;
    public LayerMask raycastLayers;
    private BoxCollider[] myColliders;

    [Header("Character model turning")]
    public Transform characterModel;
    private float facingTargetAngle;
    private float turnSmoothVelo;
    public float turnSmoothTime = 0.5f;

    [Header("Killplane")]
    public float killplaneY = -50f;

    void Awake()
    {
        mainCamera = Camera.main.transform;
        body = GetComponent<Rigidbody>();
        playerAnimateManager = GetComponent<PlayerAnimateManager>();
        otherBodyLinesHolder = GetComponent<OtherBodyLinesHolder>();
        UpdateColliderList(false);
    }

    void Start()
    {
        introTransition.SetActive(true);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetButtonDown("Jump"))        inputJumpDown = true;
        if (Input.GetButtonUp("Jump"))          inputJumpUp = true;

        ProcessFacingTargetAngle();
    }

    void FixedUpdate()
    {
        isGrounded = CheckIsGrounded();
        playerAnimateManager.SetIsGrounded(isGrounded);

        ProcessMovement();
        ProcessJumping();
        CheckIfBelowKillplane();
    }

    void OnDrawGizmos()
    {
        Vector3 killplanePosition = new Vector3(transform.position.x, killplaneY, transform.position.z);

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(killplanePosition, new Vector3(100f, 0.5f, 100f));
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, killplanePosition);
    }

    public void UpdateColliderList(bool animatePointing)
    {
        //
        // NOTE: This is because onGround will be calculated if at least one of the components is standing on the ground
        // Possible bug: if one of the components is standing on a block they are grabbing. Oh well!
        //
        myColliders = GetComponentsInChildren<BoxCollider>();

        List<Transform[]> lines = new List<Transform[]>();
        foreach (BoxCollider collider in myColliders)
        {
            if (collider.gameObject == gameObject) continue;        // NOTE: a particle system would get created on self! This prevents it
            lines.Add(new Transform[] { transform, collider.transform });
        }
        otherBodyLinesHolder.UpdateLines(lines);

        if (!animatePointing) return;
        playerAnimateManager.TriggerPoint();
    }

    private void ProcessFacingTargetAngle()
    {
        if (Time.timeScale == 0f)
        {
            characterModel.eulerAngles = new Vector3(characterModel.eulerAngles.x, facingTargetAngle, characterModel.eulerAngles.z);
            return;
        }

        //
        // Process target facing angle
        //
        float angle = Mathf.SmoothDampAngle(
            characterModel.eulerAngles.y,
            facingTargetAngle,
            ref turnSmoothVelo,
            turnSmoothTime              // NOTE: this apparently does not need any Time.deltaTime attribute to it. Huh.
        );
        characterModel.eulerAngles = new Vector3(characterModel.eulerAngles.x, angle, characterModel.eulerAngles.z);
    }

    private bool CheckIsGrounded()
    {
        foreach (BoxCollider collider in myColliders)
        {
            // NOTE: collider.bounds is world space (and is updated at the fixedupdate() range eh!)
            Vector3 bottomPosition =
                new Vector3(
                    collider.bounds.center.x,
                    collider.bounds.center.y - collider.bounds.extents.y - checkBoxHeight / 2f,
                    collider.bounds.center.z
                );

            Collider[] collidedWiths = Physics.OverlapBox(bottomPosition, collider.bounds.extents, Quaternion.identity, raycastLayers);
            foreach (Collider collidedWith in collidedWiths)
            {
                if (collidedWith.CompareTag("Player") || collidedWith.CompareTag("otherBody")) continue;

                return true;
            }
        }

        return false;
    }

    private void ProcessMovement()
    {
        //
        // Input X and Z axis movement
        //
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 targetDirection =
            Quaternion.Euler(0f, mainCamera.eulerAngles.y, 0f)
            * Vector3.ClampMagnitude(new Vector3(horizontal, 0f, vertical), 1f);

        if (targetDirection.magnitude < 0.1f)
        {
            targetDirection = Vector3.zero;
        }
        else
        {
            // Submit new facing target angle!
            facingTargetAngle = Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg;
            targetDirection = Vector3.ClampMagnitude(targetDirection, 1f);
        }

        //
        // Add velocity onto player
        //
        // JUST USE body.velocity here!!!!!!
        Vector3 flatVelocity = body.velocity;
        flatVelocity.y = 0f;                   // NOTE: for the actual flatVelocity (when we have different world up's), we'd have to take the velocity, use a quaternion to transform it to localspace up, do the stuff on here, then right before adding it in as an acceleration, we'll have to convert this back into the world up! Quaternion.FromToRotation(Vector3.up, imaNoNormalAppu);

        float attributeAmount = 1f;
        if (flatVelocity.magnitude > movementMaxXZSpeed)
        {
            float currentVelocityDotInput =
                Vector3.Dot(flatVelocity.normalized, targetDirection.normalized) / 2f + 0.5f;
            attributeAmount = (1f - currentVelocityDotInput);
        }

        Vector3 processedAcceleration =
            targetDirection *
            (isGrounded ? movementAcceleration : movementAccelerationMidair) *
            attributeAmount;
        body.AddForce(processedAcceleration * 50f * Time.deltaTime, ForceMode.Acceleration);

        // Update animation
        playerAnimateManager.SetWalkRunBlend(flatVelocity.magnitude / movementMaxXZSpeed);

        //
        // Add some deceleration against the movement
        //
        if (targetDirection.magnitude > 0.1f) return;

        Vector3 frictionedVelocity = Vector3.MoveTowards(flatVelocity, Vector3.zero, movementDeceleration * 50f * Time.deltaTime);
        body.velocity = new Vector3(frictionedVelocity.x, body.velocity.y, frictionedVelocity.z);
    }

    private void ProcessJumping()
    {
        if (inputJumpUp && !isGrounded && body.velocity.y > 0f)
        {
            // Damper the jump height
            Vector3 modVelocity = body.velocity;
            modVelocity.y *= releaseJumpDamperFactor;
            body.velocity = modVelocity;
        }
        inputJumpUp = false;

        if (!inputJumpDown) return;
        inputJumpDown = false;

        if (!isGrounded) return;

        //
        // Set velocity directly to jump! (And shift a little upwards too)
        //
        Vector3 jumpVelocity = body.velocity;
        jumpVelocity.y = jumpSpeed;

        body.MovePosition(body.position + new Vector3(0f, 0.1f, 0f));
        body.velocity = jumpVelocity;
    }

    public void SetFacingTargetAngle(float angle)
    {
        facingTargetAngle = angle;
    }

    private void CheckIfBelowKillplane()
    {
        if (transform.position.y < killplaneY)
        {
            //
            // This means die!
            //
            Vector2 randomCircle = Random.insideUnitCircle;
            SendMessage(
                "HazardContacted",
                new Vector3(
                    transform.position.x + randomCircle.x,
                    transform.position.y + 1f,
                    transform.position.z + randomCircle.y
                )
            );
        }
    }

    public Transform GetCameraTransform()
    {
        return mainCamera;
    }
}
