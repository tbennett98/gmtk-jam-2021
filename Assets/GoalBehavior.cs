﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalBehavior : MonoBehaviour
{
    public GameState gameState;
    public GameObject goalMessagePrefab;
    private bool canProceedToNextLevel = false;
    private Animator goalMessageAnimator;

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;        // NOTE: This makes it so that the player themselves has to reach the goal!

        gameState.currentCheckpointGUID = string.Empty;     // Reset so that next level won't load the checkpoint
        goalMessageAnimator =
            Instantiate(
                goalMessagePrefab,
                Vector3.zero,
                Quaternion.identity
            ).GetComponentInChildren<Animator>();
        
        Destroy(other.GetComponent<PlayerMovement>());
        Destroy(other.GetComponent<ConnectDisconnectBody>());
        Destroy(FindObjectOfType<CameraDirectionHandler>());
        other.GetComponent<Rigidbody>().velocity = Vector3.zero;
        other.GetComponent<Rigidbody>().isKinematic = true;

        canProceedToNextLevel = true;
    }

    void Update()
    {
        if (!canProceedToNextLevel) return;
        if (!Input.anyKeyDown)      return;
        canProceedToNextLevel = false;

        StartCoroutine(GotoNextLevel());
    }

    private IEnumerator GotoNextLevel()
    {
        goalMessageAnimator.SetTrigger("Transition out");

        yield return new WaitForSecondsRealtime(2f);

        //
        // Once user has hit goal and pressed <anykey>, then we go to next level!
        //
        int nextSceneId = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(nextSceneId);
    }
}
