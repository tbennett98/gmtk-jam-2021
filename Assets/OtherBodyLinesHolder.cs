﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherBodyLinesHolder : MonoBehaviour
{
    public List<Transform[]> lines;
    public GameObject particleSystemPrefab;
    private GameObject particleSystemContainerGO;

    public void UpdateLines(List<Transform[]> lines)
    {
        this.lines = lines;
        Destroy(particleSystemContainerGO);

        //
        // Instantiate all of the necessary lines and parent them to player!
        //
        particleSystemContainerGO = new GameObject("Lines Particle Systems Container");
        particleSystemContainerGO.transform.parent = transform;
        foreach (Transform[] line in lines)
        {
            GameObject lineObject = Instantiate(particleSystemPrefab, (line[0].position + line[1].position) / 2f, Quaternion.LookRotation(line[1].position - line[0].position) * Quaternion.FromToRotation(Vector3.forward, Vector3.right));
            lineObject.transform.parent = particleSystemContainerGO.transform;
        }
    }
}
