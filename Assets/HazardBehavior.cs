﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardBehavior : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        if (!other.collider.CompareTag("Player")) return;        // NOTE: if we go off the rigidbody root level, then the player could get hurt from a blue block touching spikes

        other.collider.SendMessage("HazardContacted", other.GetContact(0).point);
    }
}
