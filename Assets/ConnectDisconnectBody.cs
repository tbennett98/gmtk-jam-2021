﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectDisconnectBody : MonoBehaviour
{
    //
    // For adopting colliders without parenting transforms
    //
    public class AdoptedBodyStruct
    {
        public OtherBodyBehavior originalObject;
        public float rMass, rDrag, rAngularDrag;
        public bool rGrav, rKine;
        public RigidbodyInterpolation rInterpo;
        public CollisionDetectionMode rCollDetect;
        public RigidbodyConstraints rConstraints;
    }

    public List<AdoptedBodyStruct> adoptedBodies = new List<AdoptedBodyStruct>();

    [Header("References")]
    private Transform mainCamera;
    private PlayerMovement playerMovement;
    public GameObject defaultCursor, hasTargetCursor;

    [Header("Raycast Detection")]
    public float raycastDistance = 100f;
    public LayerMask raycastLayers;
    public string otherBodyTag = "otherBody";

    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();
    }

    void Start()
    {
        mainCamera = playerMovement.GetCameraTransform();
    }

    void Update()
    {
        {
            Ray otherBodyDetectionRay =
                new Ray(
                    mainCamera.position,
                    mainCamera.rotation * new Vector3(0f, 0f, 1f)
                );

            Debug.DrawRay(otherBodyDetectionRay.origin, otherBodyDetectionRay.direction, Color.green);
        }

        if (Input.GetButtonUp("Fire1"))
        {
            //
            // Shoot ray to another connectable body
            //
            RaycastHit otherBodyInfo;
            bool hit = CheckIfHittingTarget(out otherBodyInfo);
            if (!hit) return;

            //
            // Adopt the other colliders you can find from the other collider
            //
            AdoptedBodyStruct newBody = new AdoptedBodyStruct();
            newBody.originalObject = otherBodyInfo.collider.GetComponent<OtherBodyBehavior>();

            AdoptedBodyStruct adoptedBody = newBody.originalObject.AdoptOrUnadoptOtherBody();
            if (adoptedBody == null)
            {
                // This means that we are detaching!
                newBody.originalObject.transform.parent = null;
                RemoveCertainOtherBody(newBody.originalObject);
            }
            else
            {
                // This means we are attaching!
                adoptedBody.originalObject.transform.parent = transform;
                adoptedBodies.Add(adoptedBody);
            }

            playerMovement.UpdateColliderList(true);
        }

        //
        // If aiming for a target, then check if we're hitting
        //
        if (!Input.GetButton("Fire1")) return;

        RaycastHit _;
        bool hitCandidate = CheckIfHittingTarget(out _);
        if (hitCandidate && !hasTargetCursor.activeInHierarchy)
        {
            defaultCursor.SetActive(false);
            hasTargetCursor.SetActive(true);
        }
        else if (!hitCandidate && hasTargetCursor.activeInHierarchy)
        {
            defaultCursor.SetActive(true);
            hasTargetCursor.SetActive(false);
        }
    }

    private void RemoveCertainOtherBody(OtherBodyBehavior otherBodyBehavior)
    {
        for (int i = 0; i < adoptedBodies.Count; i++)
        {
            if (adoptedBodies[i].originalObject == otherBodyBehavior)
            {
                AdoptedBodyStruct bs = adoptedBodies[i];
                Rigidbody recreatedBody = adoptedBodies[i].originalObject.gameObject.AddComponent<Rigidbody>();

                recreatedBody.mass =                    bs.rMass;
                recreatedBody.drag =                    bs.rDrag;
                recreatedBody.angularDrag =             bs.rAngularDrag;
                recreatedBody.useGravity =              bs.rGrav;
                recreatedBody.isKinematic =             bs.rKine;
                recreatedBody.interpolation =           bs.rInterpo;
                recreatedBody.collisionDetectionMode =  bs.rCollDetect;
                recreatedBody.constraints =             bs.rConstraints;
                recreatedBody.velocity =                playerMovement.GetComponent<Rigidbody>().velocity;

                adoptedBodies.RemoveAt(i);
                return;
            }
        }
    }

    private bool CheckIfHittingTarget(out RaycastHit raycastHit)
    {
        Ray otherBodyDetectionRay =
            new Ray(
                mainCamera.position,
                mainCamera.rotation * new Vector3(0f, 0f, 1f)
            );

        return Physics.Raycast(
            otherBodyDetectionRay,
            out raycastHit,
            raycastDistance,
            raycastLayers
        ) && raycastHit.collider.CompareTag(otherBodyTag);        // Do the comparison with the collider itself, especially since there will be some 'parenting magic' happening too
    }
}
