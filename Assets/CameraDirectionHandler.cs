﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDirectionHandler : MonoBehaviour
{
    [Header("References")]
    public Cinemachine.CinemachineFreeLook defaultFreeLook;
    public Transform overTheShoulderCamera;

    [Header("Camera control props")]
    public Vector2 cameraControlSensitivity = new Vector2(1, 1);
    private Vector2 currentCamAxisValues;

    [Header("States")]
    public bool useOverTheShoulderCamera = false;

    private PlayerMovement playerMovement;

    void Awake()
    {
        playerMovement = FindObjectOfType<PlayerMovement>();
    }

    void Start()
    {
        currentCamAxisValues = new Vector2(defaultFreeLook.m_XAxis.Value, defaultFreeLook.m_YAxis.Value);
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            SwitchCamera(true);
        }

        if (Input.GetButtonUp("Fire1"))
        {
            SwitchCamera(false);
        }

        ProcessCameraMovement();
    }

    private void SwitchCamera(bool useOverTheShoulderCamera)
    {
        overTheShoulderCamera.gameObject.SetActive(useOverTheShoulderCamera);
        this.useOverTheShoulderCamera = useOverTheShoulderCamera;
        Time.timeScale = useOverTheShoulderCamera ? 0f : 1f;
    }

    private void ProcessCameraMovement()
    {
        //
        // Update the cam axis values
        //
        currentCamAxisValues.x += Input.GetAxisRaw("Mouse X") * cameraControlSensitivity.x;
        currentCamAxisValues.y += Input.GetAxisRaw("Mouse Y") * cameraControlSensitivity.y;

        currentCamAxisValues.y = Mathf.Clamp01(currentCamAxisValues.y);

        //
        // Propagate new values to the virtual cameras
        //
        if (!useOverTheShoulderCamera)
        {
            defaultFreeLook.m_XAxis.Value = currentCamAxisValues.x;
            defaultFreeLook.m_YAxis.Value = currentCamAxisValues.y;
        }
        else
        {
            overTheShoulderCamera.eulerAngles = new Vector3(currentCamAxisValues.y * 178f - 89f, currentCamAxisValues.x, 0f);
            playerMovement.SetFacingTargetAngle(overTheShoulderCamera.eulerAngles.y);
        }
    }
}
