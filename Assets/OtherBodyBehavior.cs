﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherBodyBehavior : MonoBehaviour
{
    public Material detachedMaterial;
    public Material attachedMaterial;

    public bool isAdopted = false;      // If this is true, that means that all of its colliders are taken
    private BoxCollider myCollider;
    private PhysicMaterial unadoptedPhysicMaterial;
    private Renderer myRenderer;

    void Awake()
    {
        myCollider = GetComponent<BoxCollider>();
        myRenderer = GetComponent<Renderer>();
    }

    void Start()
    {
        myRenderer.material = isAdopted ? attachedMaterial : detachedMaterial;
    }

    public ConnectDisconnectBody.AdoptedBodyStruct AdoptOrUnadoptOtherBody()
    {
        if (isAdopted)
        {
            // Unadopt!
            isAdopted = false;
            myCollider.material = unadoptedPhysicMaterial;
            myRenderer.material = detachedMaterial;

            return null;
        }

        //
        // Adopt this object!
        //
        isAdopted = true;
        unadoptedPhysicMaterial = myCollider.material;
        myCollider.material = null;
        myRenderer.material = attachedMaterial;

        ConnectDisconnectBody.AdoptedBodyStruct adoptedBody = new ConnectDisconnectBody.AdoptedBodyStruct();
        adoptedBody.originalObject = this;

        Rigidbody body = GetComponent<Rigidbody>();
        adoptedBody.rMass =         body.mass;
        adoptedBody.rDrag =         body.drag;
        adoptedBody.rAngularDrag =  body.angularDrag;
        adoptedBody.rGrav =         body.useGravity;
        adoptedBody.rKine =         body.isKinematic;
        adoptedBody.rInterpo =      body.interpolation;
        adoptedBody.rCollDetect =   body.collisionDetectionMode;
        adoptedBody.rConstraints =  body.constraints;
        Destroy(body);

        return adoptedBody;
    }
}
