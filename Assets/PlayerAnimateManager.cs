﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimateManager : MonoBehaviour
{
    public Animator playerAnimator;

    private bool previousIsGrounded;
    public void SetIsGrounded(bool flag)
    {
        if (previousIsGrounded && !flag)
        {
            playerAnimator.SetTrigger("Midair");
        }

        playerAnimator.SetBool("Is Grounded", flag);

        previousIsGrounded = flag;
    }

    public void TriggerPoint()
    {
        // NOTE: this is if a block was successfully aimed and chosen
        playerAnimator.SetTrigger("Point");
    }

    public void SetWalkRunBlend(float value)
    {
        // Value should be 0-1
        playerAnimator.SetFloat("Walk-Run Blend", value);
    }

    public void TriggerDeath()
    {
        playerAnimator.SetTrigger("Died");
    }
}
