﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteracting : MonoBehaviour
{
    public GameObject diedBangParticle;
    public GameObject diedMessageGO;

    void HazardContacted(Vector3 collisionPoint)
    {
        Debug.LogWarning("Game over! You touched a hazard");

        //
        // Deactivate everything in this object!
        //
        GetComponent<OtherBodyLinesHolder>().UpdateLines(new List<Transform[]>());      // NOTE: Send empty list so that all line holders get destroyed
        Rigidbody body = GetComponent<Rigidbody>();
        body.drag = 10f;
        body.useGravity = false;
        body.constraints = RigidbodyConstraints.None;
        body.velocity = (body.position - collisionPoint).normalized * 100f;
        body.angularVelocity = new Vector3(Random.Range(-300f, 300f), Random.Range(-300f, 300f), Random.Range(-300f, 300f));
        BoxCollider[] boxColls = GetComponentsInChildren<BoxCollider>();
        foreach (var boxColl in boxColls) { Destroy(boxColl); }
        GetComponent<PlayerAnimateManager>().TriggerDeath();
        Destroy(GetComponent<PlayerMovement>());
        Destroy(GetComponent<ConnectDisconnectBody>());
        FindObjectOfType<Cinemachine.CinemachineFreeLook>().Follow = null;

        StartCoroutine(DoHazardExplosion());
    }

    private IEnumerator DoHazardExplosion()
    {
        yield return new WaitForSeconds(1f);

        //
        // Make player disappear with a bang!
        //
        GetComponent<PlayerAnimateManager>().playerAnimator.gameObject.SetActive(false);
        FindObjectOfType<Cinemachine.CinemachineFreeLook>().LookAt = null;
        Instantiate(diedBangParticle, transform.position, diedBangParticle.transform.rotation);

        yield return new WaitForSeconds(1f);

        Instantiate(diedMessageGO, Vector3.zero, Quaternion.identity);
    }
}
