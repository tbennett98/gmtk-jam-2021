﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ContinueToNextScreen : MonoBehaviour
{
    public GameObject enableThis;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.anyKeyDown && !enableThis.activeInHierarchy)
        {
            enableThis.SetActive(true);
            StartCoroutine(GotoNextLevel());
        }
    }

    private IEnumerator GotoNextLevel()
    {
        yield return new WaitForSecondsRealtime(1f);

        //
        // Once user has hit goal and pressed <anykey>, then we go to next level!
        //
        int nextSceneId = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(nextSceneId);
    }
}
